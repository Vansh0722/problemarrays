// function find(elements,cb){
//     if (Array.isArray(elements) && elements.length > 0 && cb !== undefined && cb.constructor.name === "Function"){
//         for (let element of elements){
//             if (cb(element)){
//                 return element
//             }
//         }
//         return undefined
//     }
//     return []
    
// }
 function  myFind(items,callback){
    for (let i = 0; i < items.length; i++) {
        if (callback(items[i])){
            return (items[i])
        }
      }
      return undefined;
}

 module.exports = myFind
