 const myForeach= require("../each");
const items = require("../arrays");

function cb(element, index) {
  console.log(`index ${index} : ${element}`);
}

let result = myForeach(items,cb);
console.log(result);
