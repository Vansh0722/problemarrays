 function myReduce(items,callback, accumulator) {
    if(items.length < 1) {
        throw new Error("Array is Empty")
    }

    if(!accumulator) {
        if(typeof items[0] === "string") {
            accumulator = '';
        } else if(typeof items[0] === "number") {
            accumulator = 0;
        }
    }

    for(let index=0; index < items.length; index++) {
        accumulator = callback(accumulator, items[index],index, items);
    }
    return accumulator;
}


 module.exports = myReduce
