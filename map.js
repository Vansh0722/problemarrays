function mymap(items,callback) {
    const resultArray = [];
    for (let index = 0; index < items.length; index++) {
        resultArray.push(callback(items[index], index,items));
    }
    return resultArray;
}


module.exports = mymap

