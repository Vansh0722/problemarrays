function myFlatten(items, depth) {
  let result = [];
  if (!depth) {
    return items;
  }

  for (let i = 0; i < items.length; i++) {
    if (typeof items[i] === "undefined") {
      continue;
    }
    if ((Array.isArray(items[i]) && depth >= 1)) {
      result = result.concat(myFlatten(items[i], depth - 1));
      //console.log(result)
    } else if (items[i] === undefined || items[i] === null) {
      continue;
    } else {
      result.push(items[i]);
    }
  }
  return result;
}

module.exports = myFlatten;
